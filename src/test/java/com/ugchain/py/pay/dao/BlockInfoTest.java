package com.ugchain.py.pay.dao;

import com.ugchain.py.pay.UGTokenApplication;
import com.ugchain.py.pay.dao.bean.BlockInfo;
import com.ugchain.py.pay.dao.manager.BlockInfoManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jackliu on 17/8/3.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UGTokenApplication.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BlockInfoTest {

    @Autowired
    private BlockInfoManager manager;

    @Test
    public void insert(){
        BlockInfo info = new BlockInfo();
        info.setBlockHash("0x1");
        info.setBlockNumber(1);

        manager.insert(info);
        info.setBlockNumber(2);
        info.setBlockHash("0x2");
        manager.insert(info);

        System.out.println(manager.lastBlock());
    }

    @Test
    public void lastBlock(){
        System.out.println(manager.lastBlock());
    }
}
