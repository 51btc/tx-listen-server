package com.ugchain.py.pay;

import org.junit.Test;

import java.math.BigInteger;
import java.util.UUID;

/**
 * Created by jackliu on 17/8/1.
 */
public class T {

    @Test
    public void test(){
        BigInteger data = new BigInteger("1012345612345600000");
        System.out.println(getOrderId(data));
        System.out.println(UUID.randomUUID().toString());
    }

    private long getOrderId(BigInteger data){
        String str = data.toString();
        str = lefPaded(str,18,"0");
        int start = Math.max(0,str.length() - 18);
        String dataStr = str.substring(start + 6,12 + start);
        return Long.parseLong(dataStr);
    }

    private double getValue(BigInteger data){
        String str = data.toString();
        str = lefPaded(str,18,"0");
        StringBuilder sb = new StringBuilder();
        sb.append(str.substring(0,str.length() - 18));
        sb.append(".");
        sb.append(str.substring(str.length() - 18,str.length()));
        return Double.parseDouble(sb.toString());
    }

    private String lefPaded(String str,int len, String c){
        if(str.length() >= len){
            return str;
        }
        int sub = len - str.length();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < sub; i++){
            sb.append(c);
        }
        sb.append(str);
        return sb.toString();
    }
}
