package com.ugchain.py.pay.service;

import com.ugchain.py.pay.domain.InputData;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by fanjl on 2017/5/8.
 */
public interface Web3jService {
    void init();

    BigInteger getUgtBalance(String address);

    String transferUgt(String from,String toAddress, BigInteger value);

    InputData getTransactionById(String transactionHash);

    List<String> getTransactionHashByBlockNumber(BigInteger blockNumber);
}
