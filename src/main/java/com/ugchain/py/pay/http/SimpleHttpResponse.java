package com.ugchain.py.pay.http;

import java.net.ProtocolException;

/**
 * http client
 *
 * @author tangfulin
 * @since 16/5/4
 */
public class SimpleHttpResponse {
    private int statusCode;
    private String body;
    private Exception exception;

    private SimpleHttpResponse(int statusCode, String body) {
        this.statusCode = statusCode;
        this.body = body;
        this.exception = null;
    }

    private SimpleHttpResponse(int statusCode, String body, Exception e) {
        this.statusCode = statusCode;
        this.body = body;
        this.exception = e;
    }

    static SimpleHttpResponse Error(Exception e) {
        return new SimpleHttpResponse(500, "", e);
    }

    static SimpleHttpResponse newHttpResponse(okhttp3.Response theResponse) {
        try {
            if (theResponse.isSuccessful()) {
                return new SimpleHttpResponse(theResponse.code(), theResponse.body().string());
            } else {
                return new SimpleHttpResponse(theResponse.code(), theResponse.body().string(),
                        // TODO maybe define a new HttpException ?
                        new ProtocolException("Http StatusCode error: " + theResponse.code() + " (" + theResponse.message() + ")"));
            }
        } catch (Exception e) {
            return Error(e);
        }
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getBody() {
        return body;
    }

    public Exception getException() {
        return exception;
    }

    public boolean isSuccess() {
        return exception == null && getStatusCode() < 300;
    }

    @Override
    public String toString() {
        return "SimpleHttpResponse{" +
                "statusCode=" + statusCode +
                ", body='" + body + '\'' +
                ", exception=" + exception +
                '}';
    }
}
