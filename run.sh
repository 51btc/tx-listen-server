#!/bin/bash

if [ "$JAVA_HOME" != "" ]; then
    JAVA="$JAVA_HOME/bin/java"
else
    JAVA=java
fi
$JAVA -version
if [ "x$JAVA_OPTS" = "x" ]; then
    #JAVA_OPTS="-server -XX:+PrintGCDetails -Xloggc:gc.out -XX:+UseParallelGC -XX:ParallelGCThreads=10 -Xmx2048m -Xms2048m -Xss1024k"
    JAVA_OPTS="-server -XX:+PrintGCDetails -Dcom.sun.management.jmxremote.port=9000 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -XX:+PrintGCDateStamps -XX:+PrintTenuringDistribution -Xloggc:gc.out -XX:CMSInitiatingOccupancyFraction=80 -XX:NewSize=64m -XX:+UseConcMarkSweepGC -Xmx256m -Xms256m "
fi

KILL=kill

PIDFILE='touclick_daemon.pid'
MAIN='com.ugchain.py.pay.UGTokenApplication'
_DAEMON="ugchain.out"
CLASSPATH=$CLASSPATH:conf
for file in lib/*.jar
do
   CLASSPATH=$CLASSPATH:$file
done

case $1 in
start)

    echo -n "Starting server process in background."
    if [ -f $PIDFILE ]; then
      if kill -0 `cat $PIDFILE` > /dev/null 2>&1; then
         echo $command already running as process `cat $PIDFILE`.
         exit 0
      fi
    fi
    nohup $JAVA -cp "$CLASSPATH" $JAVA_OPTS $MAIN > "$_DAEMON" 2>&1 < /dev/null &
    if [ $? -eq 0 ]
    then
      if /bin/echo -n $! > "$PIDFILE"
      then
        sleep 1
        echo STARTED
      else
        echo FAILED TO WRITE PID
        exit 1
      fi
    else
      echo SERVER DID NOT START
      exit 1
    fi
    
    ;;
restart)

    shift
    "$0" stop ${@}
    sleep 3
    "$0" start ${@}
    ;;
stop)

    echo -n "Stopping server ... "
    if [ ! -f "$PIDFILE" ]
    then
        echo "no server to stop (could not find file $PIDFILE)"
    else
        $KILL -9 $(cat "$PIDFILE")
        rm "$PIDFILE"
        echo STOPPED
    fi
    ;;

start-foreground)

    $JAVA -cp "$CLASSPATH" $JAVA_OPTS $MAIN
    ;;
print-cmd)

    echo $JAVA -cp "$CLASSPATH" "$JAVA_OPTS" $MAIN
    ;;
*)
    echo "Usage: $0 {start|start-foreground|stop|restart|print-cmd}" >&2
esac


