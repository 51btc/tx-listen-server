package com.ugchain.py.pay.dao.mapper;

import com.ugchain.py.pay.dao.bean.BlockInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * Created by jackliu on 17/7/24.
 */
@Mapper
@Component
public interface BlockInfoMapper {
    @Insert(value = "insert into block_sync (block_number,block_hash) values " +
            "(#{blockNumber},#{blockHash})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public int insert(BlockInfo info);

    @Select(value = "SELECT * FROM block_sync order by id desc limit 1")
    @Results(
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "blockNumber", column = "block_number"),
                    @Result(property = "blockHash", column = "block_hash"),
                    @Result(property = "createTime",column = "create_time"),
            }
    )
    public BlockInfo lastBlock();


    @Select(value = "SELECT * FROM block_sync where block_number=#{blockNumber}")
    @Results(
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "blockNumber", column = "block_number"),
                    @Result(property = "blockHash", column = "block_hash"),
                    @Result(property = "createTime",column = "create_time"),
            }
    )
    public BlockInfo selectByBlockNumber(@Param("blockNumber") int blockNumber);

    @Select(value = "SELECT * FROM block_sync where block_hash=#{blockHash}")
    @Results(
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "blockNumber", column = "block_number"),
                    @Result(property = "blockNumber", column = "block_hash"),
                    @Result(property = "createTime",column = "create_time"),
            }
    )
    public BlockInfo selectByBlockHash(@Param("blockHash") String blockHash);
}
