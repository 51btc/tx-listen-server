package com.ugchain.py.pay.http;

import okhttp3.*;
import okhttp3.internal.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * httpclient util
 *
 * @author tangfulin
 * @since 16/5/4
 */
public final class HttpClientUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpClientUtil.class);

    private static final int fastTimeout = 1000;
    private static final int midTimeout = 5000;
    private static final int slowTimeout = 10000;

    // for local http calls
    private static final OkHttpClient fastHttpClient;
    // for normal outside http/https calls
    private static final OkHttpClient midHttpClient;
    // for overseas http/https calls
    private static final OkHttpClient slowHttpClient;
    // for unsafe https calls
    private static final OkHttpClient unsafeHttpClient;

    private static final HashMap<String, String> defaultHeader = new HashMap<>();

    private static final ExecutorService executorService = new ThreadPoolExecutor(2, 256, 1000, TimeUnit.MILLISECONDS,
            new SynchronousQueue<Runnable>(), Util.threadFactory("OkHttp Dispatcher", false));

    static {
        OkHttpClient.Builder fastBuilder = new OkHttpClient.Builder();
        OkHttpClient.Builder midBuilder = new OkHttpClient.Builder();
        OkHttpClient.Builder slowBuilder = new OkHttpClient.Builder();

        fastBuilder.addInterceptor(new MetricsInterceptor())
                .connectionPool(new ConnectionPool(5, 5, TimeUnit.MINUTES));
        midBuilder.addInterceptor(new MetricsInterceptor())
                .connectionPool(new ConnectionPool(5, 5, TimeUnit.MINUTES));
        slowBuilder.addInterceptor(new MetricsInterceptor())
                .connectionPool(new ConnectionPool(5, 5, TimeUnit.MINUTES));

        fastBuilder.connectTimeout(fastTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(fastTimeout, TimeUnit.MILLISECONDS)
                .writeTimeout(fastTimeout, TimeUnit.MILLISECONDS);
        fastHttpClient = fastBuilder.build();

        midBuilder.connectTimeout(midTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(midTimeout, TimeUnit.MILLISECONDS)
                .writeTimeout(midTimeout, TimeUnit.MILLISECONDS);
        midHttpClient = midBuilder.build();

        slowBuilder.connectTimeout(slowTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(slowTimeout, TimeUnit.MILLISECONDS)
                .writeTimeout(slowTimeout, TimeUnit.MILLISECONDS);
        slowHttpClient = slowBuilder.build();

        unsafeHttpClient = UnsafeHttpClient.getUnsafeHttpClient(slowTimeout);

        defaultHeader.put("User-Agent", "\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.1453.116 Safari/537.36\"");
    }

    ////======== public APIs ==========////

    ////======== simple get apis ==========////
    public static String getContent(String url) {
        return getContent(getURL(url), null, 0);
    }

    public static String getContent(String url, Map<String, String> headers) {
        return getContent(getURL(url), headers, 0);
    }

    /**
     * just ignore all charset params
     *
     * @param url
     * @param charset
     * @return utf8 response string
     */
    public static String getContent(String url, String charset) {
        String utf8Response = getContent(getURL(url), null, 0);
        return utf8Response;
        //        try {
        //            String response = new String(utf8Response.getBytes(Charset.forName("UTF-8")), Charset.forName(charset));
        //            return response;
        //        } catch (Exception e) {
        //            logger.warn("get url {} for charset {}, charset transform error", url, charset, e);
        //            return utf8Response;
        //        }
    }

    public static String getOrError(String url) throws Exception {
        return getOrError(getURL(url), null, 0);
    }

    /**
     * just ignore all charset params
     *
     * @param url
     * @param charset
     * @return utf8 response string
     */
    public static String getOrError(String url, String charset) throws Exception {
        return getOrError(getURL(url), null, 0);
    }

    public static String getOrError(String url, Map<String, String> headers) throws Exception {
        return getOrError(getURL(url), headers, 0);
    }

    public static int getStatus(String url) {
        return getStatus(getURL(url), null, 0);
    }

    /**
     * @param url
     * @param headers
     * @param timeoutMills 0 for using default timeout by client
     * @return content string, or empty string "" if error
     */
    public static String getContent(URL url, Map<String, String> headers, int timeoutMills) {
        SimpleHttpResponse response = get(url, headers, timeoutMills);
        return response.getBody();
    }

    public static int getStatus(URL url, Map<String, String> headers, int timeoutMills) {
        SimpleHttpResponse response = get(url, headers, timeoutMills);
        return response.getStatusCode();
    }

    /**
     * @param url
     * @param headers
     * @param timeoutMills 0 for using default timeout by client
     * @return content string
     * @throws Exception
     */
    public static String getOrError(URL url, Map<String, String> headers, int timeoutMills) throws Exception {
        SimpleHttpResponse response = get(url, headers, timeoutMills);
        if (response.isSuccess()) {
            return response.getBody();
        }
        throw response.getException();
    }

    ////======== simple post apis ==========////

    public static int postContentStatus(String url) {
        SimpleHttpResponse response = post(getURL(url), null, null, null, 0);
        return response.getStatusCode();
    }

    public static int postContentStatus(String url, Map<String, String> parameterMap) {
        SimpleHttpResponse response = post(getURL(url), parameterMap, null, null, 0);
        return response.getStatusCode();
    }

    public static String postContentResult(String url) {
        SimpleHttpResponse response = post(getURL(url), null, null, null, 0);
        return response.getBody();
    }

    public static String postContentResult(String url, Map<String, String> parameterMap) {
        SimpleHttpResponse response = post(getURL(url), parameterMap, null, null, 0);
        return response.getBody();
    }

    /**
     * Send post to URL.
     *
     * @param url
     * @return SimpleHttpResponse
     */
    public static SimpleHttpResponse postContent(String url) {
        return post(getURL(url), null, null, null, 0);
    }

    public static SimpleHttpResponse postContent(String url, Map<String, String> parameterMap) {
        return post(getURL(url), parameterMap, null, null, 0);
    }

    public static SimpleHttpResponse get(String url, Map<String, String> headers, int timeoutMills) {
        return get(getURL(url), headers, timeoutMills);
    }

    public static SimpleHttpResponse post(String url, Map<String, String> headers, Map<String, String> parameterMap, int timeoutMills) {
        return post(getURL(url), parameterMap, headers, timeoutMills);
    }

    ////======== the real get/post calls ==========////

    /**
     * the real get api,
     * use this directly is not encouraged
     * <p/>
     * use {@link #getContent(URL, Map, int)} if only care about the content (which will be "" when error)
     * use {@link #getOrError(URL, Map, int)} if want a Exception when error
     *
     * @param url
     * @param headers      headers
     * @param timeoutMills 0 for using default timeout by client
     * @return SimpleHttpResponse
     */
    public static SimpleHttpResponse get(URL url, Map<String, String> headers, int timeoutMills) {
        OkHttpClient client = findBestClient(url.toString(), timeoutMills);
        Map<String, String> finalHeaders = new HashMap<>();
        finalHeaders.putAll(defaultHeader);

        if (headers != null) {
            finalHeaders.putAll(headers);
        }

        Headers h = Headers.of(finalHeaders);
        Request req = new Request.Builder().url(url).headers(h).build();

        return doCall(client, req);

    }

    public static String post(String url, String body, Map<String, String> headers, int timeoutMills) throws Exception {
        SimpleHttpResponse response = post(getURL(url), body, headers, timeoutMills);
        if (response.isSuccess()) {
            return response.getBody();
        }

        throw response.getException();
    }

    /**
     * post raw body to url
     *
     * @param url
     * @param body
     * @param headers
     * @param timeoutMills
     * @return
     */
    public static SimpleHttpResponse post(URL url, String body, Map<String, String> headers, int timeoutMills) {
        OkHttpClient client = findBestClient(url.toString(), timeoutMills);

        if (headers == null) {
            headers = new HashMap<>();
        }

        // rare condition: post nothing to a url
        if (body == null) {
            body = "";
        }

        String type = "application/json; charset=utf-8";
        if (headers.containsKey("Content-type")) {
            type = headers.get("Content-type");
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse(type), body);
        Request.Builder builder = new Request.Builder().headers(Headers.of(headers)).url(url).post(requestBody);

        Request req = builder.build();
        return doCall(client, req);
    }

    public static SimpleHttpResponse post(URL url, Map<String, String> parameterMap, String username, String password, int timeoutMills) {
        if (username != null && password != null) {
            Map<String, String> headers = new HashMap<>();
            String credential = Credentials.basic(username, password);
            headers.put("Authorization", credential);
            return post(url, parameterMap, headers, timeoutMills);
        } else {
            return post(url, parameterMap, null, timeoutMills);
        }
    }

    /**
     * post parameterMap as "application/x-www-form-urlencoded"
     *
     * @param url
     * @param parameterMap
     * @param headers
     * @param timeoutMills 0 for using default timeout by client
     * @return SimpleHttpResponse
     */
    public static SimpleHttpResponse post(URL url, Map<String, String> parameterMap, Map<String, String> headers, int timeoutMills) {
        OkHttpClient client = findBestClient(url.toString(), timeoutMills);

        if (parameterMap == null) {
            parameterMap = new HashMap<>();
        }

        Map<String, String> finalHeaders = new HashMap<>();
        finalHeaders.putAll(defaultHeader);

        if (headers != null) {
            finalHeaders.putAll(headers);
        }

        FormBody.Builder fbBuilder = new FormBody.Builder();
        for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
            fbBuilder.add(entry.getKey(), entry.getValue());
        }
        RequestBody body = fbBuilder.build();
        Request.Builder builder = new Request.Builder().headers(Headers.of(finalHeaders)).url(url).post(body);

        Request req = builder.build();
        return doCall(client, req);
    }

    public static class FileData {
        /**
         * filename
         */
        private String fileName;
        /**
         * file media type
         * MediaType contentType = MediaType.parse("image/png")
         */
        private MediaType contentType;
        /**
         * file binary content
         */
        private byte[] fileDataSource;


        public byte[] getFileDataSource() {
            return fileDataSource;
        }

        public FileData setFileDataSource(byte[] fileDataSource) {
            this.fileDataSource = fileDataSource;
            return this;
        }

        public String getFileName() {
            return fileName;
        }

        public FileData setFileName(String fileName) {
            this.fileName = fileName == null ? null : fileName.trim();
            return this;
        }

        public MediaType getContentType() {
            return contentType;
        }

        public FileData setContentType(MediaType contentType) {
            this.contentType = contentType;
            return this;
        }
    }

    public static SimpleHttpResponse postByMultipartForm(String url, Map<String, String> headers, Map<String, String> parameterMap,
                                                         Map<String, List<FileData>> fileMap, int timeoutMills) {
        return postByMultipartForm(getURL(url), headers, parameterMap, fileMap, timeoutMills);
    }

    /**
     * post parameterMap/fileList as "multipart/form-data"
     *
     * @param url          the specified url
     * @param parameterMap request params map
     * @param fileMap      request file map
     * @param headers      request headers
     * @param timeoutMills timeout in mill
     * @return SimpleHttpResponse
     */
    public static SimpleHttpResponse postByMultipartForm(URL url, Map<String, String> headers, Map<String, String> parameterMap,
                                                         Map<String, List<FileData>> fileMap, int timeoutMills) {
        OkHttpClient client = findBestClient(url.toString(), timeoutMills);

        if (parameterMap == null) {
            parameterMap = new HashMap<>();
        }

        Map<String, String> finalHeaders = new HashMap<>();

        if (headers != null) {
            finalHeaders.putAll(headers);
        } else {
            finalHeaders.putAll(defaultHeader);
        }

        MultipartBody.Builder fbBuilder = new MultipartBody.Builder();
        fbBuilder.setType(MultipartBody.FORM);
        for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
            fbBuilder.addFormDataPart(entry.getKey(), entry.getValue());
        }
        if (fileMap != null) {
            for (Map.Entry<String, List<FileData>> fileParams : fileMap.entrySet()) {
                for (FileData data : fileParams.getValue()) {
                    fbBuilder.addFormDataPart(fileParams.getKey(), data.getFileName(),
                            RequestBody.create(data.getContentType(), data.getFileDataSource()));
                }
            }
        }

        MultipartBody body = fbBuilder.build();
        finalHeaders.put("Content-Type", body.boundary());
        Request.Builder builder = new Request.Builder().headers(Headers.of(finalHeaders)).url(url).post(body);

        Request req = builder.build();
        return doCall(client, req);
    }

    ////======== private functions ==========////

    /**
     * transform the checked exception to runtime exception
     *
     * @param url
     * @return
     */
    private static URL getURL(String url) {
        URL u = null;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            logger.warn("try getContent with Malformed URL {}", url);
            throw new RuntimeException("url error: " + url, e);
        }
        return u;
    }

    private static SimpleHttpResponse doCall(OkHttpClient client, Request req) {
        long start = System.nanoTime();
        boolean succ = false;
        long size = 0;
        SimpleHttpResponse response = null;

        try {
            Response res = client.newCall(req).execute();

            succ = res.isSuccessful();
            size = res.body().contentLength();

            response = SimpleHttpResponse.newHttpResponse(res);
            return response;
        } catch (IOException e) {
            logger.warn("OkHttpClient {} request {} error: {}", client, req, e.getMessage());
            response = SimpleHttpResponse.Error(e);
            return response;
        } finally {
            long cost = System.nanoTime() - start;
            if (succ) {
//                ClientAccessLogger.httpSucc(req.url().url().toString(), req.method(), cost, size);
                logger.info("url:{},method:{},cost:{},size:{}",req.url().url().toString(), req.method(), cost, size);
            } else {
//                ClientAccessLogger.httpFail(req.url().url().toString(), req.method(), cost,
//                        new RuntimeException(response.getException().toString()));
                logger.error("url:{},method:{},cost:{},exception:{}",req.url().url().toString(), req.method(), cost,
                        new RuntimeException(response.getException().toString()));
            }
        }
    }

    /**
     * auto find best client for url
     *
     * @param url
     * @param timeoutMills
     * @return
     */
    private static OkHttpClient findBestClient(String url, int timeoutMills) {
        OkHttpClient client = midHttpClient;

        // first find by timeout
        if (timeoutMills > 0) {
            if (timeoutMills <= fastTimeout) {
                client = fastHttpClient;
            } else if (timeoutMills <= midTimeout) {
                client = midHttpClient;
            } else if (timeoutMills <= slowTimeout) {
                client = slowHttpClient;
            } else {
                logger.warn("http request to {} timeoutMills {} > {} (maxTimeout) ", url, timeoutMills, slowTimeout);
                client = slowHttpClient;
            }
        } else {
            // second find by url
            if (url.contains("10.10") || url.contains("172.16") || url.contains("xueqiu.com")) {
                client = fastHttpClient;
            }
        }
        //for firstrade
        if (url.contains("firstrade.com")) {
            client = slowHttpClient;
        }

        //TODO: metrcis parts ignore
//        XueqiuMetrics.getInstance().counter("OkHttp.use." + client.connectTimeoutMillis()).inc();

        return client;
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String sendURL = "http://www.baidu.com/ns?word=" + URLEncoder.encode("茅台 | 600519", "gb2312") + "&tn=newsrss&from=news&cl=2&rn=20&ct=1";
        System.out.println(HttpClientUtil.getContent(sendURL, "gbk"));
    }

}
