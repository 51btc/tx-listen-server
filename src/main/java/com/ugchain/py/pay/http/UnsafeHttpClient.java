// Copyright (C) 2016 XueQiu
// All rights reserved

package com.ugchain.py.pay.http;

import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

/**
 * 信任所有证书的 httpClient,OkHttp3 默认配置在wifi下连接 https 站点,会偶发 SSL 握手失败的问题
 * javax.net.ssl.SSLHandshakeException:
 * sun.security.validator.ValidatorException: PKIX path building failed:
 * sun.security.provider.certpath.SunCertPathBuilderException:
 * unable to find valid certification path to requested target
 *
 *  官方 issue https://github.com/square/okhttp/issues/1582
 *
 *  使用 UnsafeHttpClient 信任所有证书可以避免上述问题;但会导致安全性下降,自行取舍.
 * @author chenhetong
 * @version 1.0
 * @created 16/5/11 下午4:41
 **/
class UnsafeHttpClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnsafeHttpClient.class);

    /**
     * trust all https certificate
     */
    private final static TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }};

    static OkHttpClient getUnsafeHttpClient(int timeoutMills) {
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory).readTimeout(timeoutMills, TimeUnit.MILLISECONDS).
                    connectTimeout(timeoutMills, TimeUnit.MILLISECONDS).writeTimeout(timeoutMills, TimeUnit.MILLISECONDS).
                    sslSocketFactory(sslSocketFactory).hostnameVerifier((s, sslSession) -> true);
            return builder.build();
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            LOGGER.error("init ssl context in UnsafeHttpClient failed", e);
            throw new RuntimeException(e);
        }
    }
}
