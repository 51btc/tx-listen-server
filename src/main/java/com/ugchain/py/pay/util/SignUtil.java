package com.ugchain.py.pay.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created by jackliu on 17/7/25.
 */
public class SignUtil {
    /**
     * 获取微信签名
     * @param payParams
     * @return
     */
    public static String getSign(Map<String, String> payParams,String appKey){
        String sign = "";
        String key = mapToString(payParams);

        String signString = key+"&key="+appKey;
        sign = MD5.getMessageDigest(signString.getBytes()).toUpperCase();
        return sign;
    }

    /**
     * 格式化待签名参数
     * @param payParams
     * @return
     */
    public static String mapToString(Map<String, String> payParams) {
        StringBuffer payParam = new StringBuffer();
        payParams = toTreeMap(payParams);
        Iterator<Map.Entry<String, String>> it = payParams.entrySet().iterator();
        int size = payParams.size();
        int mark = 0;
        while (it.hasNext()) {
            mark++;
            Map.Entry<String, String> entry = it.next();
            payParam.append(entry.getKey() + "=" + entry.getValue());
            if (mark == size) {
                break;
            }
            payParam.append("&");
        }
        return payParam.toString();
    }

    /**
     * 检查sign是否正确
     * @param sign
     * @param payParams
     * @return
     */
    public static boolean checkSign(String sign, Map<String,String> payParams,String appKey){
        String s = getSign(payParams,appKey);
        return Objects.equals(s,sign);
    }

    /**
     * Transfer to tree map
     * @param map
     * @return
     */
    public static Map<String,String> toTreeMap(Map<String,String> map){
        Map<String,String> treeMap = new TreeMap<>();
        for(String key : map.keySet()){
            treeMap.put(key,map.get(key));
        }
        return treeMap;
    }
}
