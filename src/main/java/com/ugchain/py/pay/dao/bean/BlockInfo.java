package com.ugchain.py.pay.dao.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by jackliu on 17/8/3.
 */
public class BlockInfo {
    private int id;
    private int blockNumber;
    private String blockHash;
    @JsonProperty(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8",locale = "zh")
    private Date createTime;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBlockNumber() {
        return this.blockNumber;
    }

    public void setBlockNumber(int blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getBlockHash() {
        return this.blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder("BlockInfo{");
        sb.append("blockNumber: ").append(blockNumber).append(",");
        sb.append("blockHash: ").append(blockHash).append("}");
        return sb.toString();
    }
}
