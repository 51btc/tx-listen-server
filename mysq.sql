drop database py_ug_pay;

create database py_ug_pay;

use py_ug_pay;

create table block_sync (
    id INT NOT NULL AUTO_INCREMENT,
    block_number INT NOT NULL,
    block_hash VARCHAR(80) NOT NULL,
    create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT "create time",
    PRIMARY KEY (id),
    UNIQUE KEY(block_number),
    UNIQUE KEY(block_hash)
)ENGINE=InnoDB;