package com.ugchain.py.pay.service;

import com.google.common.collect.Maps;
import com.ugchain.py.pay.config.AppConfig;
import com.ugchain.py.pay.dao.bean.BlockInfo;
import com.ugchain.py.pay.dao.manager.BlockInfoManager;
import com.ugchain.py.pay.domain.InputData;
import com.ugchain.py.pay.domain.contract.UGToken;
import com.ugchain.py.pay.http.HttpClientUtil;
import com.ugchain.py.pay.http.SimpleHttpResponse;
import com.ugchain.py.pay.util.SignUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Numeric;
import rx.*;
import rx.Observable;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.CountDownLatch;

import static org.web3j.tx.ManagedTransaction.GAS_PRICE;

/**
 * Created by fanjl on 2017/5/8.
 */
@Service
@CommonsLog
public class Web3jServiceImpl implements Web3jService {

    @Autowired
    private AppConfig appConfig;

    private Web3j web3;

    private UGToken ugToken;

    private String contractAddress;

    private String destAddress;

    @Autowired
    private BlockInfoManager manager;

    private BigInteger nextBlockNumber;

    @Override
    public void init() {
        contractAddress = appConfig.getUGTokenContractAddress().toLowerCase();
        destAddress = appConfig.getDestAddress().toLowerCase();

        web3 = Web3j.build(new HttpService(appConfig.getGethHttpUrl()));
        log.info("init web3j success! ");
        Credentials credentials = null;
        ugToken = UGToken.load(
                appConfig.getUGTokenContractAddress(), web3, credentials, GAS_PRICE, BigInteger.valueOf(4_700_000));

        if (credentials != null) {
            log.info("init web3j and load contract end, use address :" + credentials.getAddress());
        } else {
            log.warn("credentials is null ! contract can only do query operation");
        }

        nextBlockNumber = getNextBlockNumber();

        while(true){
            BigInteger last = getLastBlockNumber();
            if(nextBlockNumber.compareTo(last) >= 0){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                continue;
            }
            log.info("----Sync block number = " + nextBlockNumber.intValue());
            Request<?,EthBlock> ethBlock = web3.ethGetBlockByNumber(DefaultBlockParameter.valueOf(nextBlockNumber),true);
            try {
                EthBlock.Block block = ethBlock.send().getBlock();
                block.getTransactions().forEach(t -> {
                    Transaction tx = (Transaction)t.get();
                    handleTx(tx);
                });
                String hash = block.getHash();
                BlockInfo info = new BlockInfo();
                info.setBlockHash(hash);
                info.setBlockNumber(nextBlockNumber.intValue());
                info.setCreateTime(new Date());
                manager.insert(info);
                log.info("----Add block info ---- " + info);
                nextBlockNumber = nextBlockNumber.add(new BigInteger("1"));
            } catch (IOException e) {
                log.error("",e);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                }
                continue;
            }
        }
    }

    public BigInteger getNextBlockNumber(){
        BlockInfo info = manager.lastBlock();
        if(info != null && info.getBlockNumber() != 0){
            return new BigInteger(String.valueOf(info.getBlockNumber() + 1));
        }
        return getLastBlockNumber();
    }

    private BigInteger getLastBlockNumber(){
        try {
            return web3.ethBlockNumber().send().getBlockNumber();
        } catch (Exception e) {
            log.error("",e);
        }
        return new BigInteger("4110445");
    }


    @Override
    public BigInteger getUgtBalance(String address) {
        Address addressA = new Address(address);
        Function function = new Function("balanceOf", Arrays.<Type>asList(addressA), Collections.<TypeReference<?>>emptyList());
        String dataHex = FunctionEncoder.encode(function);
        org.web3j.protocol.core.methods.request.Transaction transaction = org.web3j.protocol.core.methods.request.Transaction.createEthCallTransaction(address, appConfig.getUGTokenContractAddress(), dataHex);
        try {
            org.web3j.protocol.core.methods.response.EthCall ethCall = web3.ethCall(transaction, DefaultBlockParameterName.LATEST).send();
            String value = ethCall.getValue();
            if (value.equals("0x")) {
                value = "0x0";
            }
            return Numeric.toBigInt(value);
        } catch (IOException e) {
            log.error("IOException while transfer ugt ", e);
        }
        return new BigInteger("0");
    }

    @Override
    public String transferUgt(String fromAddress, String toAddress, BigInteger value) {
        Address to = new Address(toAddress);
        Uint256 valueUint256 = new Uint256(value);
        Function function = new Function("transfer", Arrays.<Type>asList(to, valueUint256), Collections.<TypeReference<?>>emptyList());
        String dataHex = FunctionEncoder.encode(function);
        org.web3j.protocol.core.methods.request.Transaction transaction = org.web3j.protocol.core.methods.request.Transaction.createEthCallTransaction(fromAddress, appConfig.getUGTokenContractAddress(), dataHex);
        try {
            org.web3j.protocol.core.methods.response.EthSendTransaction ethSendTransaction = web3.ethSendTransaction(transaction).send();
            String txHash = ethSendTransaction.getTransactionHash();
            return txHash;
        } catch (IOException e) {
            log.error("IOException while transfer ugt ", e);
        }
        return "";
    }

    @Override
    public InputData getTransactionById(String transactionHash) {
        Request<?, EthTransaction> ethTransactionRequest = web3.ethGetTransactionByHash(transactionHash);
        try {
            EthTransaction ethTransaction = ethTransactionRequest.send();
            Optional<Transaction> optional = ethTransaction.getTransaction();
            if (optional.isPresent()) {
                Transaction transaction = optional.get();
                String from = transaction.getFrom();
                BigInteger blockNumber;
                try {
                    blockNumber = transaction.getBlockNumber();
                } catch (Exception e) {
                    blockNumber = BigInteger.ZERO;
                }
                String data = transaction.getInput();
                InputData inputData = new InputData();
                deserizeInput(data, inputData);
                inputData.setFrom(from);
                inputData.setBlockNumber(blockNumber);
                inputData.setSuccess(isSuccess(transactionHash));
                return inputData;
            }
        } catch (IOException e) {
            log.info("IOException", e);
        }
        return new InputData();
    }

    private boolean isSuccess(String txid) {
        Request<?, EthGetTransactionReceipt> receiptRequest = web3.ethGetTransactionReceipt(txid);
        try {
            EthGetTransactionReceipt receipt = receiptRequest.send();
            Optional<TransactionReceipt> optional = receipt.getTransactionReceipt();
            if (optional.isPresent()) {
                List<UGToken.TransferEventResponse> responses = ugToken.getTransferEvents(optional.get());
                return responses.size() > 0;
            }
        } catch (IOException e) {
            log.info("IOExcetion", e);
        }
        return false;
    }

    @Override
    public List<String> getTransactionHashByBlockNumber(BigInteger blockNumber) {
        try {
            List<String> transactionList = new LinkedList<>();
            EthBlock ethBlock = web3.ethGetBlockByNumber(DefaultBlockParameter.valueOf(blockNumber), true).send();
            List<EthBlock.TransactionResult> transactions = ethBlock.getBlock().getTransactions();
            for (EthBlock.TransactionResult transactionResult : transactions) {
                EthBlock.TransactionObject object = (EthBlock.TransactionObject) transactionResult.get();
                if (object.get().getTo().equals(appConfig.getUGTokenContractAddress().toLowerCase())) {
                    transactionList.add(object.get().getHash());
                }
            }
            return transactionList;
        } catch (IOException e) {
            log.error("IOExcepiton while get transaction hash by blocknumber", e);
        }
        return new ArrayList<>();

    }

    public void deserizeInput(String data, InputData inputData) {
        if (data.substring(0, 10).equals("0xa9059cbb")) {
            String to = data.substring(34, 74);
            String hexTo = Numeric.prependHexPrefix(to);
            String value = data.substring(74);
            BigInteger valueBigInteger = Numeric.toBigInt(value);
            inputData.setTo(hexTo);
            inputData.setValue(valueBigInteger);
        } else {
            log.info("not transfer tx");
        }
    }

    private void handleTx(Transaction tx) {
        String from = tx.getFrom();
        String to = tx.getTo();
        String input = tx.getInput();
        if(to != null && to.toLowerCase().equals(contractAddress)){
            InputData inputData = new InputData();
            inputData.setFrom(from);
            deserizeInput(input,inputData);
            log.info("transfer ugt transaction:{From: " + from +
                    ",To: " + inputData.getTo() +
                    ",Value: " + inputData.getValue() + "}");

            if(inputData.getTo().toLowerCase().equals(destAddress)) {
                send(inputData);
            }
        }
    }

    private void send(InputData data){
        long orderId = getOrderId(data.getValue());
        double value = getValue(data.getValue());
        Map<String,String> params = new TreeMap<>();
        params.put("orderId",String.valueOf(orderId));
        params.put("value",String.valueOf(value));
        params.put("address",data.getFrom());
        String sign = SignUtil.getSign(params,appConfig.getPrivateKey());
        params.put("sign",sign);

        SimpleHttpResponse resp = HttpClientUtil.get(url(appConfig.getNotifyUrl(),params), Maps.newHashMap(),4000);

        if(resp.isSuccess()) {
            log.info("----------Notify----------" + params+
            ", isSuccess=true, response= " + resp.getBody());
        }else{
            log.info("----------Notify----------" + params+
                    ", isSuccess=false");
        }
    }

    private String url(String baseUrl,Map<String,String> params){
        StringBuilder sb = new StringBuilder(baseUrl);
        sb.append("/orderId/").append(params.get("orderId"));
        sb.append("/value/").append(params.get("value"));
        sb.append("/address").append(params.get("address"));
        sb.append("/sign/").append(params.get("sign")).append(".html");
        return sb.toString();
    }

    private long getOrderId(BigInteger data){
        String str = data.toString();
        str = lefPaded(str,18,"0");
        int start = Math.max(0,str.length() - 18);
        String dataStr = str.substring(start + 6,12 + start);
        return Long.parseLong(dataStr);
    }

    private double getValue(BigInteger data){
        String str = data.toString();
        str = lefPaded(str,18,"0");
        StringBuilder sb = new StringBuilder();
        sb.append(str.substring(0,str.length() - 18));
        sb.append(".");
        sb.append(str.substring(str.length() - 18,str.length()));
        return Double.parseDouble(sb.toString());
    }

    private String lefPaded(String str,int len, String c){
        if(str.length() >= len){
            return str;
        }
        int sub = len - str.length();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < sub; i++){
            sb.append(c);
        }
        sb.append(str);
        return sb.toString();
    }
}