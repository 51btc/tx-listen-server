package com.ugchain.py.pay;

import com.ugchain.py.pay.service.Web3jServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class UGTokenApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(UGTokenApplication.class, args);
		context.getBean(Web3jServiceImpl.class).init();

	}
}
