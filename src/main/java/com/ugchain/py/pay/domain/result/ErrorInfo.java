package com.ugchain.py.pay.domain.result;

import lombok.Getter;

public enum ErrorInfo {

    ADDRESS_ERROR(1001,"创建私钥文件错误"),
    SERVER_ERROR(9001,"服务器错误");
    @Getter
    private int errorCode;
    @Getter
    private String errorMessage;

    private ErrorInfo(int code, String message) {
        this.errorCode = code;
        this.errorMessage = message;
    }

}
