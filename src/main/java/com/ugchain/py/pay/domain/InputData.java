package com.ugchain.py.pay.domain;

import lombok.Data;

import java.math.BigInteger;

/**
 * Created by fanjl on 2017/7/4.
 */
@Data
public class InputData {
    private String from;
    private String to;
    private BigInteger value;
    private BigInteger blockNumber;
    private boolean isSuccess;
}
