package com.ugchain.py.pay.dao.manager;

import com.ugchain.py.pay.dao.bean.BlockInfo;
import com.ugchain.py.pay.dao.mapper.BlockInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by jackliu on 17/7/26.
 */

@Component
public class BlockInfoManager {

    /** Logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(BlockInfoManager.class);

    @Autowired
    private BlockInfoMapper mapper;

    /**
     * Insert
     * @param blockInfo
     * @return
     */
    public int insert(BlockInfo blockInfo){
        try{
            return mapper.insert(blockInfo);
        }catch (Exception e){
            LOGGER.error("insert into blockInfo error. " + blockInfo,e);
        }
        return 0;
    }

    /**
     * Get last block
     * @return
     */
    public BlockInfo lastBlock(){
        try{
            return mapper.lastBlock();
        }catch (Exception e){
            LOGGER.error("get last block for error.",e);
        }
        return null;
    }

    /**
     * Select by block number
     * @param blockNumber
     * @return
     */
    public BlockInfo selectByBlockNumber(int blockNumber){
        try{
            return mapper.selectByBlockNumber(blockNumber);
        }catch (Exception e) {
            LOGGER.error("select by block number error.",e);
        }
        return null;
    }

    /**
     *
     * @param hash
     * @return
     */
    public BlockInfo selectByBlockHash(String hash) {
        try{
            return mapper.selectByBlockHash(hash);
        }catch (Exception e){
            LOGGER.error("select by block hash error.",e);
        }
        return null;
    }
}
