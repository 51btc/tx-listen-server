package com.ugchain.py.pay.controller;

import com.ugchain.py.pay.config.AppConfig;
import com.ugchain.py.pay.domain.InputData;
import com.ugchain.py.pay.util.ResultUtil;
import com.ugchain.py.pay.domain.result.Result;
import com.ugchain.py.pay.service.Web3jService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by fanjl on 2017/6/13.
 */
@RestController
@CommonsLog
@RequestMapping
public class IndexController {


    @Autowired
    private AppConfig appConfig;
    @Autowired
    private Web3jService web3jService;

    @RequestMapping(value = "/getBalance")
    public Result getBalance(
            @RequestParam("address") String address
    ) {
        BigInteger value = web3jService.getUgtBalance(address);
        return ResultUtil.success(value);
    }

    @RequestMapping(value = "/transfer")
    public Result transfer(
            @RequestParam("from") String from,
            @RequestParam("to") String to,
            @RequestParam("value") String value
    ) {
        String hash = web3jService.transferUgt(from,to,new BigInteger(value));
        return ResultUtil.success(hash);
    }

    @RequestMapping(value = "/getTransactionByHash")
    public Result getTransactionByHash(
            @RequestParam("transactionHash") String transactionHash
    ) {
        InputData data = web3jService.getTransactionById(transactionHash);
        return ResultUtil.success(data);
    }

    @RequestMapping(value = "/getTransactionHashByBlockNumber")
    public Result getByBlockNumber(
            @RequestParam("blockNumber") String blockNumber
    ) {
        List<String> txHash = web3jService.getTransactionHashByBlockNumber(new BigInteger(blockNumber));
        return ResultUtil.success(txHash);
    }
}
