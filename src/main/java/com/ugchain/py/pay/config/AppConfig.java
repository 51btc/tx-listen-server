package com.ugchain.py.pay.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by yuanshichao on 2016/12/15.
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "web")
public class AppConfig {


    private String gethHttpUrl;

    private String UGTokenContractAddress;

    private String destAddress;

    private String privateKey;

    private String notifyUrl;

}
